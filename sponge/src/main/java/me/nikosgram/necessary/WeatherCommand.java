/*
 * Copyright (c) 2015 Nikos Grammatikos
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package me.nikosgram.necessary;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import org.spongepowered.api.entity.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.Texts;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.util.command.CommandException;
import org.spongepowered.api.util.command.CommandResult;
import org.spongepowered.api.util.command.CommandSource;
import org.spongepowered.api.util.command.args.CommandContext;
import org.spongepowered.api.util.command.spec.CommandExecutor;
import org.spongepowered.api.world.World;
import org.spongepowered.api.world.weather.Weather;

@RequiredArgsConstructor(access = AccessLevel.PROTECTED)
public class WeatherCommand implements CommandExecutor {
    private final NecessarySponge necessary;

    @Override
    public CommandResult execute(CommandSource sender, CommandContext context) throws CommandException {
        World world;
        if (!context.hasAny("world")) {
            if (sender instanceof Player) {
                world = ((Player) sender).getWorld();
            } else {
                throw new CommandException(Texts.builder("This command using only from players.").color(TextColors.RED).build());
            }
        } else {
            world = context.<World>getOne("world").get();
        }
        Weather weather = context.<Weather>getOne("weather").get();
        world.forecast(weather);
        sender.sendMessage(
                Texts.builder("You set the weather to ").color(TextColors.GRAY).append(
                        Texts.builder(weather.getName()).color(TextColors.RED).build())
                        .append(Texts.builder(" in ").color(TextColors.GRAY).build()).append(
                        Texts.builder(
                                world.getName()).color(TextColors.RED).build())
                        .append(Texts.builder(".").color(TextColors.GRAY).build()).build());
        Text who = sender instanceof Player ? ((Player) sender).getDisplayNameData().getDisplayName() :
                Texts.builder(sender.getName()).color(TextColors.RED).build();
        for (Player notify : necessary.server.getOnlinePlayers()) {
            if (notify.hasPermission("necessary.weather.notify")) {
                if (!notify.getName().equals(sender.getName())) {
                    notify.sendMessage(
                            Texts.builder("The weather changed to ").color(TextColors.GRAY).append(
                                    Texts.builder(weather.getName()).color(TextColors.RED).build())
                                    .append(Texts.builder(" in ").color(TextColors.GRAY).build()).append(
                                    Texts.builder(world.getName()).color(TextColors.RED).build())
                                    .append(Texts.builder(" from the ").color(TextColors.GRAY).build()).append(who)
                                    .append(Texts.builder(".").color(TextColors.GRAY).build()).build());
                }
            }
        }
        return CommandResult.success();
    }
}