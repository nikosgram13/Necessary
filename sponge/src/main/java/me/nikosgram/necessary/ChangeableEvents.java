/*
 * Copyright (c) 2015 Nikos Grammatikos
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package me.nikosgram.necessary;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import org.spongepowered.api.event.Subscribe;
import org.spongepowered.api.event.entity.player.PlayerJoinEvent;
import org.spongepowered.api.event.entity.player.PlayerQuitEvent;
import org.spongepowered.api.event.weather.WeatherChangeEvent;
import org.spongepowered.api.text.Texts;
import org.spongepowered.api.world.weather.Weathers;

@RequiredArgsConstructor(access = AccessLevel.PROTECTED)
public class ChangeableEvents {
    private final NecessarySponge necessary;

    @Subscribe
    public void onPlayerJoinChangeMessage(PlayerJoinEvent event) {
        if (necessary.config.getNode("custom", "enable").getBoolean()) {
            String message = necessary.config.getNode("custom", "join-message").getString();
            if (!(message.isEmpty() || message.equalsIgnoreCase("none"))) {
                event.setNewMessage(Texts.of());
            }
        }
    }

    @Subscribe
    public void onPlayerQuitChangeMessage(PlayerQuitEvent event) {
        if (necessary.config.getNode("custom", "enable").getBoolean()) {
            String message = necessary.config.getNode("custom", "quit-message").getString();
            if (!(message.isEmpty() || message.equalsIgnoreCase("none"))) {
                event.setNewMessage(Texts.of());
            }
        }
    }

    @Subscribe
    public void onWeatherChange(WeatherChangeEvent event) {
        if (event.getResultingWeather().equals(Weathers.THUNDER_STORM)) {
            if (!necessary.config.getNode("weather", "thunder").getBoolean()) {
                event.setResultingWeather(event.getInitialWeather());
            }
        } else if (event.getResultingWeather().equals(Weathers.RAIN)) {
            if (!necessary.config.getNode("weather", "rain").getBoolean()) {
                event.setResultingWeather(event.getInitialWeather());
            }
        }
    }
}