/*
 * Copyright (c) 2015 Nikos Grammatikos
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package me.nikosgram.necessary;

import com.google.inject.Inject;
import lombok.Getter;
import me.nikosgram.necessary.api.NecessaryPlugin;
import me.nikosgram.necessary.api.WarpManager;
import ninja.leaping.configurate.ConfigurationNode;
import ninja.leaping.configurate.commented.CommentedConfigurationNode;
import ninja.leaping.configurate.loader.ConfigurationLoader;
import org.slf4j.Logger;
import org.spongepowered.api.Game;
import org.spongepowered.api.Server;
import org.spongepowered.api.entity.player.gamemode.GameMode;
import org.spongepowered.api.event.Subscribe;
import org.spongepowered.api.event.state.InitializationEvent;
import org.spongepowered.api.event.state.PreInitializationEvent;
import org.spongepowered.api.plugin.Plugin;
import org.spongepowered.api.service.config.DefaultConfig;
import org.spongepowered.api.text.Texts;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.util.command.CommandException;
import org.spongepowered.api.util.command.CommandResult;
import org.spongepowered.api.util.command.CommandSource;
import org.spongepowered.api.util.command.InvocationCommandException;
import org.spongepowered.api.util.command.args.CommandContext;
import org.spongepowered.api.util.command.args.GenericArguments;
import org.spongepowered.api.util.command.spec.CommandExecutor;
import org.spongepowered.api.util.command.spec.CommandSpec;
import org.spongepowered.api.world.weather.Weather;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

@Plugin(id = "necessary", name = "Necessary", version = "1.0.1-R0.1-SNAPSHOT")
public class NecessarySponge implements NecessaryPlugin {
    private final Properties properties;
    @Getter
    private final WarpManager warps = new NecessaryWarpManager(this);
    @Inject
    protected Game game;
    protected Server server;
    @Inject
    protected Logger logger;
    @Inject
    @DefaultConfig(sharedRoot = true)
    protected File configFile;
    @Inject
    @DefaultConfig(sharedRoot = true)
    protected ConfigurationLoader<CommentedConfigurationNode> configManager;
    protected ConfigurationNode config;

    public NecessarySponge() {
        InputStream stream = this.getClass().getResourceAsStream("/.properties");
        properties = new Properties();
        try {
            properties.load(stream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static <T> List<T> page(int page, int size, List<T> list) {
        List<T> returned = new ArrayList<T>();
        page -= 1;

        if ((size * (page)) > list.size()) return returned;

        for (int i = (size * page);
             i < ((size * page) + size > list.size() ? list.size() : (size * page) + size); i++) {
            returned.add(list.get(i));
        }

        return returned;
    }

    @Subscribe
    public void onPreInitialization(PreInitializationEvent event) {
        server = game.getServer();
        try {
            if (!configFile.exists()) {
                Files.createFile(configFile.toPath());
                config = configManager.load();

                config.getNode("version").setValue(1);

                config.getNode("world", "enable").setValue(true);
                config.getNode("world", "perPermission").setValue(true);

                config.getNode("warp", "enable").setValue(true);
                config.getNode("warp", "perPermission").setValue(true);

                config.getNode("home", "enable").setValue(true);
                config.getNode("home", "multiple", "default").setValue(1);
                config.getNode("home", "multiple", "vip").setValue(2);

                config.getNode("chat", "enable").setValue(true);
                config.getNode("chat", "radius").setValue(0);
                config.getNode("chat", "format", "default").setValue("{name}&7 > {message}");
                config.getNode("chat", "format", "owner").setValue("&7{world} {group} {name}&7 > {message}");

                config.getNode("economy", "enable").setValue(true);
                config.getNode("economy", "starting-balance").setValue(0);
                config.getNode("economy", "currency-symbol").setValue('$');
                config.getNode("economy", "max-money").setValue(100000);
                config.getNode("economy", "min-money").setValue(-100);

                config.getNode("disable", "pvp").setValue(false);
                config.getNode("disable", "damage").setValue(false);
                config.getNode("disable", "damage", "fall").setValue(false);
                config.getNode("disable", "damage", "fire").setValue(false);
                config.getNode("disable", "damage", "lava").setValue(false);
                config.getNode("disable", "damage", "drown").setValue(false);
                config.getNode("disable", "damage", "wither-explosion").setValue(false);
                config.getNode("disable", "damage", "contact").setValue(false);
                config.getNode("disable", "damage", "suffocate").setValue(false);
                config.getNode("disable", "damage", "lightning").setValue(false);
                config.getNode("disable", "damage", "projectiles").setValue(false);
                config.getNode("disable", "spawn", "passive-mobs").setValue(false);
                config.getNode("disable", "spawn", "passive-mobs", "bat").setValue(false);
                config.getNode("disable", "spawn", "passive-mobs", "chicken").setValue(false);
                config.getNode("disable", "spawn", "passive-mobs", "cow").setValue(false);
                config.getNode("disable", "spawn", "passive-mobs", "mooshroom").setValue(false);
                config.getNode("disable", "spawn", "passive-mobs", "pig").setValue(false);
                config.getNode("disable", "spawn", "passive-mobs", "rabbit").setValue(false);
                config.getNode("disable", "spawn", "passive-mobs", "sheep").setValue(false);
                config.getNode("disable", "spawn", "passive-mobs", "squid").setValue(false);
                config.getNode("disable", "spawn", "passive-mobs", "villager").setValue(false);
                config.getNode("disable", "spawn", "neutral-mobs").setValue(false);
                config.getNode("disable", "spawn", "neutral-mobs", "cave-spider").setValue(false);
                config.getNode("disable", "spawn", "neutral-mobs", "enderman").setValue(false);
                config.getNode("disable", "spawn", "neutral-mobs", "spider").setValue(false);
                config.getNode("disable", "spawn", "neutral-mobs", "pig-zombie").setValue(false);
                config.getNode("disable", "spawn", "hostile-mobs").setValue(false);
                config.getNode("disable", "spawn", "hostile-mobs", "blaze").setValue(false);
                config.getNode("disable", "spawn", "hostile-mobs", "creeper").setValue(false);
                config.getNode("disable", "spawn", "hostile-mobs", "guardian").setValue(false);
                config.getNode("disable", "spawn", "hostile-mobs", "endermite").setValue(false);
                config.getNode("disable", "spawn", "hostile-mobs", "ghast").setValue(false);
                config.getNode("disable", "spawn", "hostile-mobs", "magma-cube").setValue(false);
                config.getNode("disable", "spawn", "hostile-mobs", "silverfish").setValue(false);
                config.getNode("disable", "spawn", "hostile-mobs", "skeleton").setValue(false);
                config.getNode("disable", "spawn", "hostile-mobs", "slime").setValue(false);
                config.getNode("disable", "spawn", "hostile-mobs", "witch").setValue(false);
                config.getNode("disable", "spawn", "hostile-mobs", "wither-skull").setValue(false);
                config.getNode("disable", "spawn", "hostile-mobs", "zombie").setValue(false);
                config.getNode("disable", "spawn", "tamable-mobs").setValue(false);
                config.getNode("disable", "spawn", "tamable-mobs", "horse").setValue(false);
                config.getNode("disable", "spawn", "utility-mobs").setValue(false);
                config.getNode("disable", "spawn", "utility-mobs", "iron-golem").setValue(false);
                config.getNode("disable", "spawn", "utility-mobs", "snowman").setValue(false);
                config.getNode("disable", "spawn", "boss-mobs").setValue(false);
                config.getNode("disable", "spawn", "boss-mobs", "ender-dragon").setValue(false);
                config.getNode("disable", "spawn", "boss-mobs", "wither").setValue(false);
                config.getNode("disable", "spawn", "unused-mobs").setValue(false);
                config.getNode("disable", "spawn", "unused-mobs", "giant").setValue(false);

                config.getNode("weather", "rain").setValue(true);
                config.getNode("weather", "thunder").setValue(true);
                config.getNode("weather", "lightning").setValue(true);

                config.getNode("newbies", "enable").setValue(true);
                config.getNode("newbies", "spawn").setValue("none");
                config.getNode("newbies", "kit").setValue("starter");
                config.getNode("newbies", "announce").setValue("&7Welcome {name}&7 to the server!");

                config.getNode("respawn", "priority").setValue("high");
                config.getNode("respawn", "at-home").setValue(true);

                config.getNode("custom", "enable").setValue(true);
                config.getNode("custom", "join-message").setValue("none");
                config.getNode("custom", "quit-message").setValue("none");

                configManager.save(config);
                logger.info("Created default configuration, Necessary will not run until you have edited this file!");
            }
        } catch (IOException exception) {
            logger.error("Couldn't create default configuration file!");
        }

        game.getEventManager().register(this, new CancelableEvents(this));
        game.getEventManager().register(this, new ChangeableEvents(this));
    }

    @Subscribe
    public void onInitialization(InitializationEvent event) {
        game.getCommandDispatcher().register(
                this, CommandSpec.builder().description(Texts.of("Teleport directly to a player.")).permission(
                        "necessary.teleportpos")
                        .arguments(
                                GenericArguments
                                        .onlyOne(GenericArguments.location(Texts.of("location"), game)),
                                GenericArguments.optional(GenericArguments.player(Texts.of("target"), game)))
                        .executor(new TeleportPositionCommand(this)).build(), "teleportpos", "tppos");

        game.getCommandDispatcher().register(
                this, CommandSpec.builder().description(Texts.of("Teleport directly to a player.")).permission(
                        "necessary.teleport")
                        .arguments(
                                GenericArguments.onlyOne(GenericArguments.player(Texts.of("target"), game)),
                                GenericArguments.optional(GenericArguments.player(Texts.of("to"), game)))
                        .executor(new TeleportCommand(this)).build(), "teleport", "tp");

        game.getCommandDispatcher().register(
                this, CommandSpec.builder().description(Texts.of("Defines a new warp location.")).permission(
                        "necessary.createwarp").arguments(
                        GenericArguments.onlyOne(GenericArguments.string(Texts.of("warp")))
                ).executor(new CreateWarpCommand(this)).build(), "setwarp", "addwarp", "createwarp");

        game.getCommandDispatcher().register(
                this,
                CommandSpec.builder().description(Texts.of("Removes a warp location by warp name.")).permission(
                        "necessary.removewarp")
                        .arguments(
                                GenericArguments.onlyOne(GenericArguments.string(Texts.of("warp")))
                        ).executor(new RemoveWarpCommand(this)).build(),
                "delwarp",
                "removewarp",
                "remwarp",
                "rmwarp");

        game.getCommandDispatcher().register(
                this, CommandSpec.builder().description(Texts.of("Warps you to a pre-set location.")).permission(
                        "necessary.warp")
                        .arguments(
                                GenericArguments.onlyOne(GenericArguments.string(Texts.of("warp"))),
                                GenericArguments.optional(GenericArguments.player(Texts.of("player"), game)))
                        .executor(new WarpCommand(this)).build(), "warp");

        game.getCommandDispatcher().register(
                this, CommandSpec.builder().description(Texts.of("Change player gamemode.")).permission(
                        "necessary.gamemode").arguments(
                        GenericArguments.onlyOne(
                                GenericArguments.catalogedElement(Texts.of("mode"), game, GameMode.class)),
                        GenericArguments.optional(GenericArguments.player(Texts.of("player"), game)))
                        .executor(new GamemodeCommand(this)).build(), "gamemode", "gm");

        game.getCommandDispatcher().register(
                this, CommandSpec.builder().description(Texts.of("Manipulates the weather.")).permission(
                        "necessary.weather").arguments(
                        GenericArguments.onlyOne(
                                GenericArguments.catalogedElement(Texts.of("weather"), game, Weather.class)),
                        GenericArguments.optional(GenericArguments.world(Texts.of("world"), game)))
                        .executor(new WeatherCommand(this)).build(), "weather");

        game.getCommandDispatcher().register(
                this, CommandSpec.builder().description(Texts.of("Manipulates the weather.")).permission(
                        "necessary.weathers").arguments(
                        GenericArguments.optional(GenericArguments.integer(Texts.of("page"))))
                        .executor(new WeatherListCommand(this)).build(), "weathers");

        game.getCommandDispatcher().register(
                this, CommandSpec.builder().description(Texts.of("Manipulates the weather.")).permission(
                        "necessary.gamemodes").arguments(
                        GenericArguments.optional(GenericArguments.integer(Texts.of("page"))))
                        .executor(new GamemodeListCommand(this)).build(), "gamemodes");

        game.getCommandDispatcher().register(
                this, CommandSpec.builder().permission("necessary.command").description(
                        Texts.of("Access to necessary command.")).child(
                        CommandSpec.builder().description(Texts.of("Reload the config.")).permission(
                                "necessary.reload").executor(
                                new CommandExecutor() {
                                    @Override
                                    public CommandResult execute(CommandSource sender, CommandContext context)
                                            throws CommandException {
                                        try {
                                            configManager.load();
                                            sender.sendMessage(
                                                    Texts.builder(
                                                            "Necessary reloaded.").color(TextColors.GRAY).build());
                                        } catch (IOException e) {
                                            throw new InvocationCommandException(
                                                    Texts.builder(
                                                            "Some problem prevented the Necessary to " +
                                                                    "reload the configuration file.").color(TextColors.RED)
                                                            .build(), e);
                                        }
                                        return CommandResult.success();
                                    }
                                }).build(),
                        "reload").child(
                        CommandSpec.builder().description(Texts.of("Returns Necessary version.")).permission(
                                "necessary.version").executor(
                                new CommandExecutor() {
                                    @Override
                                    public CommandResult execute(CommandSource sender, CommandContext context)
                                            throws CommandException {
                                        sender.sendMessage(
                                                Texts.builder("Necessary ").color(TextColors.BLUE).append(
                                                        Texts.builder(
                                                                game.getPluginManager().getPlugin(
                                                                        "necessary").get()
                                                                        .getVersion()).color(TextColors.RED).build())
                                                        .build(),
                                                Texts.builder("by nikosgram13").color(TextColors.YELLOW).build());
                                        return CommandResult.success();
                                    }
                                })
                                .build(), "version", "ver").build(),
                "necessary", "nec");
    }

    @Override
    public String getVersion() {
        return properties.getProperty("version");
    }

    @Override
    public String getVersionName() {
        return properties.getProperty("version-name");
    }

    @Override
    public int getProtocol() {
        return (int) properties.get("protocol");
    }
}