/*
 * Copyright (c) 2015 Nikos Grammatikos
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package me.nikosgram.necessary;

import com.google.common.base.Optional;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import me.nikosgram.necessary.api.Warp;
import org.spongepowered.api.entity.player.Player;
import org.spongepowered.api.text.Texts;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.util.command.CommandException;
import org.spongepowered.api.util.command.CommandPermissionException;
import org.spongepowered.api.util.command.CommandResult;
import org.spongepowered.api.util.command.CommandSource;
import org.spongepowered.api.util.command.args.CommandContext;
import org.spongepowered.api.util.command.spec.CommandExecutor;

@RequiredArgsConstructor(access = AccessLevel.PROTECTED)
public class WarpCommand implements CommandExecutor {
    private final NecessarySponge necessary;

    @Override
    public CommandResult execute(CommandSource sender, CommandContext context) throws CommandException {
        Player player;
        if (!context.hasAny("player")) {
            if (sender instanceof Player) {
                player = (Player) sender;
            } else {
                throw new CommandException(Texts.builder("This command using only from players.").color(TextColors.RED).build());
            }
        } else {
            if (!sender.hasPermission("necessary.gamemode.other")) {
                throw new CommandPermissionException();
            } else {
                player = context.<Player>getOne("player").get();
            }
        }
        Optional<Warp> warp = necessary.getWarps().getWarp(context.<String>getOne("warp").get());
        if (warp.isPresent()) {
            if (player.hasPermission("necessary.teleport.unsafely")) {
                player.setLocation(warp.get().getLocation());
                player.sendMessage(Texts.of("Teleported."));
                //TODO:message
            } else if (player.setLocationSafely(warp.get().getLocation())) {
                player.sendMessage(Texts.of("Teleported."));
                //TODO:message
            } else {
                player.sendMessage(Texts.of("Failed."));
                //TODO:message
            }
        } else {
            throw new CommandException(Texts.builder("I can't find this warp.").color(TextColors.RED).build());
        }
        return CommandResult.success();
    }
}