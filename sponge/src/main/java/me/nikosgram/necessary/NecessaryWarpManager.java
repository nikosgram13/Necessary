/*
 * Copyright (c) 2015 Nikos Grammatikos
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package me.nikosgram.necessary;

import com.google.common.base.Optional;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import me.nikosgram.necessary.api.Warp;
import me.nikosgram.necessary.api.WarpManager;
import org.spongepowered.api.world.Location;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

@RequiredArgsConstructor(access = AccessLevel.PROTECTED)
public class NecessaryWarpManager implements WarpManager {
    private final NecessarySponge necessary;
    private final Map<String, Warp> warps = new HashMap<String, Warp>();

    @Override
    public Optional<Warp> getWarp(String name) {
        if (warps.containsKey(name)) {
            return Optional.of(warps.get(name));
        }
        return Optional.absent();
    }

    @Override
    public Optional<Warp> createWarp(String name, Location location) {
        if (!warps.containsKey(name)) {
            Warp warp = new NecessaryWarp(name, location);
            warps.put(name, warp);
            //TODO: update the database.
            return Optional.of(warp);
        }
        return Optional.absent();
    }

    @Override
    public boolean removeWarp(String name) {
        if (warps.containsKey(name)) {
            warps.remove(name);
            //TODO: update the database.
            return true;
        }
        return false;
    }

    @Override
    public Iterator<Warp> iterator() {
        return warps.values().iterator();
    }
}
